class H{
    public float n;

    public void f(int g){
        System.out.println("Metoda f a clasei H, cu parametru de tip int");
    }
}

class I extends H{
    private long t;
    private K obiectK;

    public void f(){
        System.out.println("Metoda f a clasei I");
    }
}

class J{
    public void i(I l){
        System.out.println("Metoda i din Clasa J foloseste un element de clasa I");
    }
}

class K{
    private L l;
    private M m;

    public K(M m){
        this.l = new L();
        this.m = m;
    }
}

class L{
    public void metA(){
        System.out.println("Metoda clasei L");
    }
}

class M{
    public void metB(){
        System.out.println("Metoda clasei M");
    }
}


public class SubiectulUnu {
    public static void main(String[] args) {

    }
}
