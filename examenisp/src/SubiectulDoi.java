import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class UInterface extends JFrame{
    JTextArea nChars, aWrite;
    JButton doButton;

    public UInterface(){
        init();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(600,500);
        setVisible(true);
    }

    public void init(){
        this.setLayout(null);

        nChars = new JTextArea();
        nChars.setBounds(10,10,550, 150);
        add(nChars);

        aWrite = new JTextArea();
        aWrite.setBounds(10,180,550,150);
        add(aWrite);

        doButton = new JButton("Press it!");
        doButton.setBounds(10, 350,200,100);
        doButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                aWrite.setText("");
                int N = nChars.getText().length();
                for(int i = 0; i < N; i++){
                    aWrite.append("a");
                }
            }
        });
        add(doButton);

    }
}


public class SubiectulDoi {
    public static void main(String[] args) {
        new UInterface();
    }
}
